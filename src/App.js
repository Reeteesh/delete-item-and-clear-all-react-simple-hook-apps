import React from 'react'

const App = () => {
  const [data, setData] = React.useState([
    {
      id: 1,
      name: "Ritesh",
    },
    {
      id: 2,
      name: "Rajina",
    },
    {
      id: 3,
      name: "Rahul",
    },
  ]);
  const displayData = data.map(item => {
    return <div key={item.id}>
      <button onClick={() => deleteName(item.id)}>Delete</button>{item.name}</div>
  })
  const deleteName = (id) => {
    setData((oldData) => {
      const newData = oldData.filter(x => x.id !== id);
      return newData;
    })

  }
  return (
    <div>
      {displayData}
      <button onClick={() => setData([])}>Clear All</button>
    </div>
  )
}

export default App
